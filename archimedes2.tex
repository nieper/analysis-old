% Created 2016-11-07 Mo 15:05
\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{color}
\usepackage{listings}
\usepackage{geometry}
\usepackage[germanb]{babel}
\usepackage{mathtools}
\author{Prof.~Dr.~Marc Nieper-Wißkirchen}
\date{\today}
\title{Berechnung der Bogenlänge des Einheitskreises über eine Sehne nach Archimedes}
\hypersetup{
 pdfauthor={Prof.~Dr.~Marc Nieper-Wißkirchen},
 pdftitle={Berechnung der Bogenlänge des Einheitskreises über eine Sehne nach Archimedes},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 24.5.1 (Org mode 8.3.3)}, 
 pdflang={Germanb}}
\begin{document}

\maketitle

\section{Das Verfahren des Archimedes'}
\label{sec:orgheadline1}

Wir verwenden die Bezeichnungen aus Aufgaben 9 und 12.  Die Beziehung 
\(B_n = n \cdot b_n\) wird durch die Scheme-Prozedur  
\begin{minted}[]{scheme}
  (define (b->B b n)
    (* n b))
\end{minted}
implementiert.

Lösung des Teils (a) aus Aufgabe 9 ergibt die Rekursionsformel
\begin{equation}
\label{eq:orglatexenvironment1}
b_{2^{k + 1}} = \sqrt{2 - 2 \sqrt{1 - b_{2^k}^2/4}}
\end{equation}
mit \(b_1 = \ell\).

Wir können uns an dieser Stelle eine Quadratwurzel sparen, indem wir \(r_{2^k} \coloneqq
b_{2^k}^2/4\) setzen.  Eine Scheme-Prozedur, die diese Umrechnung vornimmt ist:
\begin{minted}[]{scheme}
  (define (b->r b)
    (* 1/4 b b))
\end{minted}
Die Umkehrung ist durch
\begin{minted}[]{scheme}
  (define (r->b r)
    (* 2 (sqrt r)))
\end{minted}
gegeben.

Mittels dieser Setzung wird aus (\ref{eq:orglatexenvironment1}): 
\[ 
r_{2^{k + 1}} = \frac 1 2
\Bigl(1 - \sqrt{1 - r_{2^k}}\Bigr).  
\] 
Auf der rechten Seite tritt
die in der Aufgabenstellung erwähnte Auslöschung auf, so daß wir gemäß
dem Tip die Rekursionsformel in die Form
\[
r_{2^{k + 1}} = \frac 1 2 \frac {r_{2^k}} {1 + \sqrt{1 - r_{2^k}}}
\]
bringen.

Eine Scheme-Prozedur, die die Rekursion implementiert, ist etwa:
\begin{minted}[]{scheme}
  (define (r_n->r_2n r_n)
    (* 1/2 (/ r_n (+ 1 (sqrt (- 1 r_n))))))
\end{minted}

Insgesamt \(k\) Rekursionsschritte führen wir in folgender Prozedur aus:
\begin{minted}[]{scheme}
  (define (r_1->r_2^k r_1 k)
    (let rekursionsschritt ((k k) (r r_1))
      (if (zero? k)
          r
          (rekursionsschritt (- k 1) (r_n->r_2n r)))))
\end{minted}

Um die Bogenlänge über eine Sehne \(\ell\) in \(k\) Schritten zu
bestimmen, können wir also schreiben:
\begin{minted}[]{scheme}
  (define (B l k)
    (let* ((r_1 (b->r l))
           (r_2^k (r_1->r_2^k r_1 k))
           (b_2^k (r->b r_2^k))
           (B_2^k (b->B b_2^k (expt 2 k))))
      B_2^k))
\end{minted}

Zur Demonstration führen wir \(30\) Rekursionsschritte für \(\ell = 2\) aus:
\begin{minted}[]{scheme}
  (B 2.0 30)
\end{minted}

\begin{verbatim}
3.1415926535897936
\end{verbatim}

Zum Vergleich geben wir den erwarteten Wert, nämlich \(\pi\) im Rahmen der Maschinengenauigkeit an:
\begin{minted}[]{scheme}
  (atan 0.0 -1.0)
\end{minted}

\begin{verbatim}
3.141592653589793
\end{verbatim}

Wir sehen, daß unser Verfahren eine sehr gute Approximation an die Kreiszahl liefert!

\section{Das Verfahren als eine Intervallschachtelung}
\label{sec:orgheadline2}

Aus Teil (a) von Aufgabe 12 wissen wir weiter, daß
\[
t_n = \sqrt{\frac{b_n^2}{1 - b_n^2/4}}.
\]
Setzen wir also \(s_n \coloneqq t_n^2/4\), so erhalten wir
\[
s_n = \frac{r_n}{1 - r_n},
\]
bzw.~als Scheme-Prozedur:
\begin{minted}[]{scheme}
  (define (r->s r)
    (/ r (- 1.0 r)))
\end{minted}

Da wir erwarten, daß die Berechnung der Bogenlänge um so besser wird,
je kleiner die Differenz zwischen \(s_n - r_n\) wird, formulieren wir die
Abbruchbedingung um.  Zunächst schreiben wir:
\begin{minted}[]{scheme}
  (define (r_1->r r_1 eps)
    (let rekursionsschritt ((n 1) (r r_1))
      (let ((s (r->s r)))
        (if (< (- s r) eps)
            (values r n)
            (rekursionsschritt (* n 2) (r_n->r_2n r))))))
\end{minted}
Diese Funktion liefert zwei Werte zurück; die Approximation an \(r\) und
die Anzahl der Segmente im letzten Approximationsschritt.

Die eigentliche Berechnung der Bogenlänge wird dann zu
\begin{minted}[]{scheme}
 (define (B l eps)
    (let* ((r_1 (b->r l)))
      (call-with-values
          (lambda () (r_1->r r_1 eps))
	(lambda (r n)
	  (let* ((b (r->b r))
		 (B (b->B b n)))
	    B)))))
\end{minted}
umdefiniert.

Wählen wir eine sinnvolle Fehlerschranke:
\begin{minted}[]{scheme}
  (define epsilon 1e-30)
\end{minted}
Damit können wir unser Verfahren testen:
\begin{minted}[]{scheme}
  (B 2.0 epsilon)
\end{minted}

\begin{verbatim}
3.1415926535897936
\end{verbatim}

\section{Das Verfahren zur Berechnung des Arkussinus}
\label{sec:orgheadline3}

Nach Teil (b) von Aufgabe 12 besteht der Zusammenhang
\[
  \arcsin x = \frac B 2,
\]
wenn \(x = \frac s 2\) die halbe Bogenlänge ist.  Für negative Werte von \(x\) gilt
\[
\arcsin (-x) = - \arcsin x,
\]
so daß folgende Prozedur den Arkussinus berechnet:
\begin{minted}[]{scheme}
(define (arcsin x eps)
  (if (negative? x)
      (arcsin (- x) eps)
      (* 1/2 (B (* 2 x) eps))))
\end{minted}

Ein kleiner Test:
\begin{minted}[]{scheme}
(arcsin 0.5 epsilon)
\end{minted}

\begin{verbatim}
0.5235987755982987
\end{verbatim}

Im Vergleich dazu liefert Schemes Standard-Prozedur:
\begin{minted}[]{scheme}
(asin 0.5)
\end{minted}

\begin{verbatim}
0.5235987755982989
\end{verbatim}
\end{document}