\chapter{Der Körper $\set R$ der reellen Zahlen}

Eine \emph{reelle Zahl} ist per definitionem ein Element einer Menge
(mit Zusatzstruktur) $\set R$, welche den in den folgenden beiden
Kapiteln angegebenen Axiomen (R1)--(R13) genügt.  Damit ist nicht
konkret festgelegt, was eine einzelne reelle Zahl ist.  Es wird aber
genau festgelegt, welche Beziehungen zwischen den reellen Zahlen
bestehen und wie wir mit reellen Zahlen umzugehen haben.  Die Menge
$\set R$ ist durch die Axiome (R1)--(R13) im wesentlichen eindeutig
bestimmt.  Die Existenz einer solchen Menge postulieren wir als Axiom.
(Es gibt Konstruktionsverfahren der Menge der reellen Zahlen die
zeigen, daß es ausreicht, die Existenz der Menge der natürlichen
Zahlen zu postulieren.  Aus dieser läßt sich dann die Menge $\set R$ konstruieren.)

Der Sinn der meisten Aussagen dieses Kapitels ist, dem
Leser\footnote{Es handelt sich hierbei offensichtlich um ein
  \emph{generisches Maskulinum} und hat mit dem biologischen
  Geschlecht, dem \emph{Sexus}, der angesprochenen Menge von Menschen
  nichts zu tun.  Mit anderen Worten kann das biologische Geschlecht
  von "`Leser"' männlich oder weiblich sein.  Da der Mathematik das
  biologische Geschlecht reichlich egal ist, werde ich weiterhin die
  grammatikalisch jeweils einfachste Form verwenden.} zu versichern, daß die
axiomatisch eingeführte Menge $\set R$ die ihm vertrauten
Eigenschaften hat.  Insbesondere werden wir die Mengen $\set N_0$ der
natürlichen Zahlen, $\set Z$ der ganzen Zahlen und $\set Q$ der
rationalen Zahlen in $\set R$ als Teilmengen entdecken.

\section{Die Axiome der Addition}

Auf den Elementen der Menge $\set R$ ist eine \emph{Addition}
definiert, d.\,h.\ eine Zuordnung, die jedem Paar $(a, b)$ von
Elementen $a$, $b \in \set R$ ein Element $a + b \in \set R$
zuordnet, welches die \emph{Summe} von $a$ und $b$ heißt.

Weiter gibt es in $\set R$ ein ausgezeichnetes Element $0 \in \set R$,
welches \emph{Null} heißt.

Zusätzlich ist auf den Elementen der Menge $\set R$ eine Zuordnung
definiert, die jedem Element $a \in \set R$ ein Element $-a$ zuordnet,
welches das \emph{Negative} von $a$ heißt.

\begin{namedthm}{Axiome der Addition}
  Für alle $a$, $b$, $c \in \set R$ gilt:
  \begin{axiomlist}[label=(R\arabic*)]
  \item
    \label{it:real_addition_assoc}
    $a + (b + c) = (a + b) + c$.
  \item $a + 0 = a = 0 + a$.
  \item $a + (-a) = 0 = (-a) + a$.
  \item 
    \label{it:real_addition_comm}
    $a + b = b + a$.
  \end{axiomlist}
  Das Axiom~\ref{it:real_addition_assoc} heißt auch das \emph{Assoziativgesetz} der Addition, und
  Axiom~\ref{it:real_addition_comm} heißt auch das \emph{Kommutativgesetz} der Addition.
\end{namedthm}

\begin{proposition}
  In $\set R$ existiert genau eine Null. Das bedeutet: Ist $n \in \set R$ mit $\forall a \in \set R:
  a + n = a$, so folgt schon $n = 0$.
\end{proposition}

\begin{proposition}
  In $\set R$ existiert zu jedem Element $a \in \set R$ genau ein
  Negatives, d.\,h.~ist $b \in \set R$ mit $a + b = 0$, so folgt schon
  $b = -a$.
\end{proposition}

\begin{definition*}
  Anstatt $a + (-b)$ schreiben wir $a - b$.
\end{definition*}

\begin{rules*}
  Für alle $a$, $b \in \set R$ gilt:
  \begin{enumerate}
  \item $-(-a) = a$.
  \item $-(a + b) = (-a) + (-b)$.
  \item $a = -a \iff a = 0$.
  \end{enumerate}
\end{rules*}

\begin{warning*}
  Die Implikation $a = -a \implies a = 0$ folgt nicht allein aus den
  bisher vorgestellten Axiomen.  Um sie zu beweisen, benötigen wir
  auch die Anordnungsaxiome (R10)--(R12).
\end{warning*}

\begin{theorem*}
  Für jedes Paar $(a, b)$ von Elementen $a$, $b \in \set R$ hat die
  Gleichung der Form $a + x = b$ genau eine Lösung $x \in \set R$, nämlich
  $x = b - a$.
\end{theorem*}

\section{Die Axiome der Multiplikation}

Auf den Elementen der Menge $\set R$ ist eine \emph{Multiplikation}
definiert, d.\,h.\ eine Zuordnung, die jedem Paar $(a, b)$ von
Elementen $a$, $b \in \set R$ ein Element $a b = a \cdot b \in \set R$
zuordnet, welches das \emph{Produkt} von $a$ und $b$ heißt.

Weiter gibt es in $\set R^* = \set R \setminus \{0\}$
ein ausgezeichnetes Element $1 \in \set R^*$,
welches \emph{Eins} heißt.

Zusätzlich ist auf den Elementen der Menge $\set R^*$ eine Zuordnung
definiert, die jedem Element $a \in \set R$ ein Element $a^{-1}$ zuordnet,
welches das \emph{Inverse} von $a$ heißt.

\begin{namedthm}{Axiome der Multiplikation}
  Für alle $a$, $b$, $c \in \set R$ gilt:
  \begin{axiomlist}[label=(R\arabic*), start=5]
  \item
    \label{it:real_multiplication_assoc}
    $a \cdot (b \cdot c) = (a \cdot b) \cdot c$.
  \item $a \cdot 1 = a = 1 \cdot a$.
  \item $a \cdot a^{-1} = 1 = a^{-1} \cdot a$, wenn $a \in \set R^*$
  \item 
    \label{it:real_multiplication_comm}
    $a \cdot b = b \cdot a$.
  \end{axiomlist}
  Das Axiom~\ref{it:real_multiplication_assoc} heißt auch das
  \emph{Assoziativgesetz} der Multiplikation, und
  Axiom~\ref{it:real_multiplication_comm} heißt auch das
  \emph{Kommutativgesetz} der Multiplikation.
\end{namedthm}

\begin{proposition}
  In $\set R^*$ existiert genau eine Eins. Das bedeutet: Ist $e \in \set R^*$ mit $\forall a \in \set R:
  a \cdot e = a$, so folgt schon $e = 1$.
\end{proposition}

\begin{proposition}
  In $\set R^*$ existiert zu jedem Element $a \in \set R^*$ genau ein
  Inverses, d.\,h.~ist $b \in \set R^*$ mit $a \cdot b = 1$, so folgt schon
  $b = a^{-1}$.
\end{proposition}

\begin{definition*}
  Anstatt $a \cdot (b^{-1})$ schreiben wir $a b^{-1}$, $\frac a b$ oder $a/b$.
\end{definition*}

\begin{rules*}
  Für alle $a$, $b \in \set R^*$ gilt:
  \begin{enumerate}
  \item $(a^{-1})^{-1} = a$.
  \item $(a b)^{-1} = b^{-1} \cdot a^{-1}$.
  \item 
    \label{it:self_inverse_reals}
    $a = a^{-1} \iff a \in \{1, -1\}$.
  \end{enumerate}
\end{rules*}

\begin{warning*}
  Obige Äquivalenz~\ref{it:self_inverse_reals} folgt nicht allein aus den
  bisher vorgestellten Axiomen.  Um sie zu beweisen, benötigen wir
  auch das Distributivgesetz (R9).
\end{warning*}

\begin{theorem*}
  Für jedes Paar $(a, b)$ von Elementen $a \in \set R^*$, $b \in \set R$ hat die
  Gleichung $a \cdot x = b$ genau eine Lösung $x \in \set R$, nämlich
  $x = b/a$.
\end{theorem*}

\section{Das Distributivgesetz}

Das Distributivgesetz stellt die Verbindung zwischen Addition und Multiplikation her.

\begin{namedthm}{Distributivgesetz}
  Für alle $a$, $b$, $c \in \set R$ gilt
  \begin{axiomlist}[label=(R\arabic*), start=9]
  \item
    $a \cdot (b + c) = (a \cdot b) + (a \cdot c)$.
  \end{axiomlist}
\end{namedthm}

\begin{rules*}
  Für alle $a$, $b \in \set R$ gilt:
  \begin{enumerate}
  \item
    \label{it:multiplication_by_zero}
    $a \cdot 0 = 0$.
  \item
    $(-a) \cdot b = - (a b)$.
  \item
    $(-a)^2 = a^2$.
  \item
    \label{it:reals_form_a_domain}
    $ab = 0 \iff (a = 0 \lor b = 0)$.
  \end{enumerate}
\end{rules*}

Die obige Regel~\ref{it:reals_form_a_domain} formuliert die sogenannte
\emph{Nullteilerfreiheit} von $\set R$.

\begin{warning*}
  Wegen obiger Regel~\ref{it:multiplication_by_zero} und $0 \neq 1$
  ist $0^{-1}$ ein verbotener Ausdruck.  Bei jeder Division müssen wir
  also sicherstellen, daß wir nicht durch Null teilen.  Diese
  Vorschrift zu beherzigen ist mitunter gar nicht so einfach, z.\,B.\
  wenn wir durch den Wert einer Funktion teilen wollen, deren Nullstellen wir
  nicht kennen.
\end{warning*}

\begin{comment*}
  Die bisher eingeführten Axiome (R1)--(R9) besagen gerade, daß
  $\set R$ bezüglich der Addition und Multiplikation ein \emph{Körper}
  ist.
\end{comment*}

\section{Die Anordnungsaxiome}

In $\set R^*$ gibt es eine Teilmenge $\set R_+$, deren Elemente die
\emph{positiven} reellen Zahlen heißen.
\begin{namedthm}{Anordnungsaxiome}
  Für alle $a$, $b$, $c \in \set R$ gilt
  \begin{axiomlist}[label=(R\arabic*), start=10]
  \item 
    \label{it:real_units_as_disjoint_union}
    $a \in \set R^* \implies a \in \set R_+ \lor -a \in \set R_+$.
  \item
    $a \in \set R_+ \land b \in \set R_+
    \implies a + b \in \set R_+$.
  \item
    $a \in \set R_+ \land b \in \set R_+
    \implies a \cdot b \in \set R_+$.    
  \end{axiomlist}
\end{namedthm}
Schreiben wir
$\set R_- \coloneqq - \set R_+ = \{-a \mid a \in \set R_+\}$, so
können wir Axiom~\ref{it:real_units_as_disjoint_union} auch in der
Form $\set R^* = \set R_+ \cup \set R_-$ schreiben.  Die Elemente von
$\set R_-$ heißen die \emph{negativen} reellen Zahlen.

\begin{proposition}
  Es gilt $\set R = \set R_- \disjointunion \{0\} \disjointunion \set R_+$; in
  Worten: $\set R$ ist die \emph{disjunkte} Vereinigung von
  $\set R_-$, $\{0\}$, $\set R_+$, das heißt $\set R$ ist die
  Vereinigung dieser drei Teilmengen und je zwei dieser drei
  Teilmengen haben einen leeren Durchschnitt.
\end{proposition}

\begin{definition}
  Für $a$, $b \in \set R$ definieren wir:
  \begin{align*}
    a < b    & \coloniff b - a \in \set R_+, \tag{$a$ ist \emph{kleiner} als $b$} \\
    a \leq b & \coloniff a < b \lor a = b,   \tag{$a$ ist \emph{kleiner-gleich} $b$} \\
    a > b    & \coloniff b < a,              \tag{$a$ ist \emph{größer} als $b$} \\
    a \geq b & \coloniff a > b \lor a = b.   \tag{$a$ ist \emph{größer-gleich} $b$}
  \end{align*}
  Wir sehen, daß gilt: $\text{$a$ ist positiv} \iff a > 0$ und $\text{$a$ ist negativ} \iff a < 0$.
\end{definition}
  
\begin{theorem*}
  \leavevmode
  \begin{enumerate}
  \item
    Für jedes Paar $(a, b)$ reeller Zahlen $a$, $b \in \set R$ gilt genau eine der folgenden
    Aussagen:
    \[
    a < b,\quad a = b,\quad\text{oder}\quad a > b.
    \]
    Wir sagen auch, daß "`$<$"' eine \emph{vollständige} Ordnung auf $\set R$ definiert.
  \item
    Für alle $a$, $b$, $c \in \set R$ gilt: $a < b \land b < c \implies a < c$.
  \item
    Für alle $a$, $b$, $c \in \set R$ gilt: $a < b \implies a + c < b + c$.
  \item
    Für alle $a$, $b$, $c \in \set R$ gilt: $a < b \land 0 < c \implies ac < bc$.
  \end{enumerate}
\end{theorem*}

\begin{rules*}
  Für alle $a$, $b$, $c$, $d \in \set R$ gilt:
  \begin{enumerate}
  \item
    $a < b \land c < d \implies a + c < b + d$,
  \item
    $a < b \iff -b < -a$,
  \item
    $a < b \land c < 0\implies bc < ac$,
  \item
    $0 < ab \iff (a > 0 \land b > 0) \lor (a < 0 \land b < 0)$,
  \item
    $a \neq 0 \iff 0 < a^2$; insbesondere gilt $0 < 1$,
  \item
    $0 < a < b \implies 0 < 1/b < 1/a$.
  \end{enumerate}
\end{rules*}

\begin{definition}
  Unter der \emph{Vorzeichenfunktion} oder dem \emph{Signum} verstehen wir die Funktion
  \[
  \sign\colon \set R \to \{-1, 0, 1\}, t \mapsto \begin{cases}
    1 & \text{für $t \in \set R_+$}, \\
    0 & \text{für $t = 0$}, \\
    -1 & \text{für $t \in \set R_-$}.
  \end{cases}
  \]
  Damit ist auch definiert, was es heißt, wenn wir sagen, daß zwei
  reelle Zahlen das gleiche Vorzeichen haben.
\end{definition}

\begin{proposition}
  Für alle $a$, $b \in \set R$ gilt:
  \begin{align*}
    \sign (\sign (a)) & = \sign (a), \\
    \sign (-a) & = - \sign (a) \\
    \intertext{und}
    \sign (a \cdot b) & = \sign (a) \cdot \sign (b).
  \end{align*}
\end{proposition}

\begin{corollary*}
  Für alle $a$, $b \in \set R^*$ gilt:
  \[
  a \cdot b > 0 \iff \sign(a) = \sign(b).
  \]
\end{corollary*}

\section{Der Betrag reeller Zahlen}

\begin{definition*}
  Für jedes $a \in \set R$ heißt
  \[
  \abs a \coloneqq \sign(a) \cdot a
  \]
  der \emph{(Absolut-)Betrag} von $a$.
\end{definition*}
Offenbar gilt
\[
\abs a = \begin{cases}
  a, & \text{falls $a \ge 0$}, \\
  -a, & \text{falls $a < 0$}.
\end{cases}
\]

\begin{rules*}
  Sind $a$, $b$, $r \in \set R$ und $r \ge 0$, so gilt:
  \begin{enumerate}
  \item
    $\abs a \ge 0 \land \bigl(\abs a = 0 \iff a = 0\bigr)$,
  \item
    $\abs{-a} = \abs a$,
  \item
    $\abs{ab} = \abs a \cdot \abs b$,
  \item
    $\abs{1/a} = 1/\abs a$, falls $a \neq 0$,
  \item
    $- \abs a \leq a \leq \abs a$,
  \item
    $\bigl(\abs a < r \iff -r < a < r\bigr) \land \bigl(\abs a \leq r
    \iff -r \leq a \leq r\bigr)$,
  \item
    $\bigl(\abs{a - b} < r \iff b - r < a < b + r\bigr) \land
    \bigl(\abs{a - b} \leq r \iff b - r \leq a \leq b + r\bigr)$,
  \item
    \label{it:triangle_inequality}
    $\abs{a \pm b} \leq \abs a + \abs b$,
  \item
    $\abs{\abs a - \abs b} \leq \abs{a \pm b}$.
  \end{enumerate}
\end{rules*}
Obige Regel~\ref{it:triangle_inequality} heißt die \emph{Dreiecksungleichung}.

\begin{exercise*}
  Es seien $n$, $m \in \set N_0$ natürliche Zahlen, $a_0$, \dots,
  $a_n$, $b_0$, \dots, $b_m \in \set R$ reelle Zahlen mit
  $a_n = b_m = 1$ und $P$ und $Q$ die "`Polynomfunktionen"'
  \[
  P\colon \set R \to \set R, t \mapsto \sum_{k = 0}^n a_k \cdot t^k\quad\text{bzw.}\quad
  Q\colon \set R \to \set R, t \mapsto \sum_{k = 0}^m b_k \cdot t^k.
  \]
  Dann gilt:
  \begin{enumerate}
  \item
    Es gibt eine Zahl $R \in \set R_+$, so daß für alle $t \in \set R$ mit $\abs t \ge R$ gilt:
    \[
    \frac 1 2 \cdot {\abs t}^n \leq \abs{P(t)} \leq 2 \cdot {\abs t}^n.
    \]
    (Tip: Man braucht nur $\abs t \ge 1$ zu betrachten. Ist
    $M \coloneqq \sum_{k = 0}^{n - 1} \abs{a_k}$, so kann man
    $R \coloneqq \max\{1, 2 M\}$ wählen.)
  \item Zu der "`gebrochen-rationalen"' Funktion $f \coloneqq P/Q$
    gibt es eine Zahl $R \in \set R_+$, so daß für alle $t \in \set R$ mit $\abs t \ge R$ gilt:
    \[
    \frac 1 4 \cdot {\abs t}^{n - m} \leq \abs{f(t)} \leq 4 \cdot {\abs t}^{n - m}.
    \]
  \end{enumerate}
\end{exercise*}

\section{Intervalle}

\begin{definition*}
  Für reelle Zahlen $a$, $b \in \set R$ mit $a < b$ definieren wir
  \begin{align*}
    \tag{abgeschlossenes, beschränktes Intervall}
    [a, b] & \coloneqq \{t \in \set R \mid a \leq t \leq b\}, \\
    \tag{halboffene, beschränkte Intervalle}
    \begin{split}
      \left[a, b\right[ & \coloneqq \{t \in \set R \mid a \leq t < b\}, \\
      \left]a, b\right] & \coloneqq \{t \in \set R \mid a < t \leq b\},
    \end{split}
    \\
    \tag{offenes, beschränktes Intervall}
    \left]a, b\right[ & \coloneqq \{t \in \set R \mid a < t < b\}, 
    \\
    \tag{abgeschlossene, unbeschränkte Intervalle}
    \begin{split}
      \left[a, \infty\right[ & \coloneqq \{t \in \set R \mid a \leq t\},
      \\
      \left]-\infty, b\right] & \coloneqq \{t \in \set R \mid t \leq b\},
    \end{split}
    \\
    \tag{offene, unbeschränkte Intervalle}
    \begin{split}
      \left]a, \infty\right[ & \coloneqq \{t \in \set R \mid a < t\},
      \\
      \left]-\infty, b\right[ & \coloneqq \{t \in \set R \mid t < b\}.
    \end{split}
  \end{align*}

  Ist $a = b$, so sind $[a, b] \coloneqq \{a\}$ und
  $\left[a, b\right[ \coloneqq \left]a, b\right] \coloneqq \left]a,
    b\right[ \coloneqq \emptyset$
  sogenannte \emph{entartete Intervalle}; $\{a\}$ wird auch als ein
  abgeschlossenes, beschränktes Intervall betrachtet.
\end{definition*}

\section{Die Menge $\set N_0$ der natürlichen Zahlen}
\label{sec:naturals}

Wir finden im angeordneten Körper $\set R$ eine Menge $\set N_0$,
welche genau die Peano-Axiome aus \ref{sec:peano} erfüllt.
\begin{theorem}
  Sei $\tilde S\colon \set R \to \set R, a \mapsto a + 1$. Dann
  existiert genau eine Teilmenge $\set N_0 \subseteq \set R$ mit
  $\tilde S(\set N_0) \subseteq \set N_0$, so daß für das Tripel
  $(\set N_0, S, 0)$, in welchem $S$ die Einschränkung
  \[
  S \coloneqq \tilde S|\set N_0\colon \set N_0 \to \set N_0, a
  \mapsto a +1
  \]
  bezeichnet, die Aussagen (N1)--(N4) aus~\ref{sec:peano} gelten.  Und
  zwar ist $\set N_0$ der Durchschnitt aller Teilmengen
  $N \subseteq \set R$ (und damit die kleinste dieser Teilmengen $N$),
  für welche gilt:
  \[
  0 \in N\quad\text{und}\quad\tilde S(N) \subseteq N.
  \]
\end{theorem}

In Zukunft benutzen wir die Elemente dieser so konstruierten Menge
natürlicher Zahlen zum Durchnumerieren von Folgen.  Insbesondere
verwenden wir in diesem Sinne die vollständige Induktion und rekursive
Definition.

\begin{rules*}
  Sind $n$, $m \in \set N_0$, so gilt:
  \begin{enumerate}
  \item $0 \leq n < n + 1$,
  \item $n + m$, $n \cdot m \in \set N_0$,
  \item $n \leq m \iff m - n \in \set N_0$,
  \item $\lnot (n < m < n + 1)$.
  \end{enumerate}
\end{rules*}

\section{Der Ring $\set Z$ der ganzen Zahlen}
\label{sec:integers}

\begin{definition*}
  Die Elemente von $\set Z \coloneqq \set N_0 \cup (-\set N_0)$ heißen die \emph{ganzen} Zahlen.
\end{definition*}

\begin{comment*}
  Die Menge $\set Z$ ist die kleinste Untergruppe der additiven Gruppe
  von $\set R$, die die $1$ enthält.  In $\set Z$ ist die
  Multiplikation uneingeschränkt ausführbar.
\end{comment*}

Wie die natürlichen Zahlen haben wir also die ganzen Zahlen nicht
erfunden, sondern als spezielle reelle Zahlen gefunden.

\section{Der angeordnete Körper $\set Q$ der rationalen Zahlen}
\label{sec:rationals}

\begin{definition*}
  Eine Zahl $a \in \set R$ heißt \emph{rational}, wenn es $p$,
  $q \in \set Z$ mit $q > 0$ gibt, so daß $a = p/q$.  Mit $\set Q$
  bezeichnen wir die Menge der rationalen Zahlen.
\end{definition*}

\begin{comment*}
  Die Menge $\set Q$ ist der kleinste Unterkörper von $\set R$. In ihm
  gelten also die Axiome (R1)--(R9), wenn wir jeweils $\set R$ durch
  $\set Q$ und $\set R^*$ durch $\set Q^*$ ersetzen.  Weiterhin gelten
  in $\set Q$ ebenfalls die Anordnungsaxiome (R10)--(R12), wenn wir
  $\set R_+$ durch $\set Q_+ \coloneqq \set Q \cap \set R_+$ ersetzen.
  Folglich gelten mit den entsprechenden Modifikationen auch alle aus
  den Axiomen bisher abgeleiteten Aussagen.  Außerdem können wir
  bisher offensichtlich nicht $\set Q = \set R$ ausschließen.  Erst
  mit dem noch fehlenden Axiom (R13) wird erzwungen, daß
  $\set Q \subsetneq \set R$, daß also $\set Q \neq \set R$.
\end{comment*}

\section{Erweiterung der Zahlengeraden mit $\infty$ und $-\infty$}

\begin{definition*}
  Sei $\widehat{\set R}$ eine Menge, die aus $\set R$ durch Hinzunahme
  zweier verschiedener Elemente $\infty$, $-\infty \notin \set R$
  entsteht.  Dann erweitern wir die Rechenoperationen von $\set R$ auf
  $\widehat{\set R}$ für $a \in \set R$ wie folgt:
  \begin{align*}
    a + \infty & \coloneqq \infty + a \coloneqq \infty, \\
    a - \infty & \coloneqq (- \infty) + a \coloneqq a + (-\infty) \coloneqq - \infty, \\
    \infty + \infty & \coloneqq \infty - (- \infty) \coloneqq \infty, \\
    - \infty + (- \infty) & \coloneqq (-\infty) - \infty \coloneqq -\infty, \\
    -(\infty) & \coloneqq -\infty, \\
    \abs \infty & \coloneqq \abs{-\infty} \coloneqq \infty, \\
    \frac a \infty & \coloneqq \frac a {-\infty} \coloneqq 0.
  \end{align*}

  Für $a \in \set R_+$ sei
  \begin{align*}
    a \cdot \infty & \coloneqq \infty \cdot a \coloneqq (-a) \cdot (-\infty) \coloneqq
    (-\infty) \cdot (-a) \coloneqq \infty \cdot \infty \coloneqq (- \infty) \cdot (- \infty)
    \coloneqq \infty, \\
    (-a) \cdot \infty & \coloneqq \infty \cdot (-a) \coloneqq a \cdot (-\infty) \coloneqq
    (-\infty) \cdot a \coloneqq \infty \cdot (-\infty) \coloneqq (- \infty) \cdot \infty
    \coloneqq -\infty
  \end{align*}
  
Weiterhin wird die Anordnung $<$ von $\set R$ auf $\widehat{\set R}$ durch die Festsetzung
  \[
  -\infty < a < \infty\quad\text{und insbesondere}\quad
  -\infty < \infty
  \]
  für $a \in \set R$ fortgesetzt.

  Schließlich definieren wir für jedes $a \in \set R$ die unbeschränkten Intervalle
  \begin{align*}
    [a, \infty] & \coloneqq \left[a, \infty\right[ \cup \{\infty\}, \\
    \left]a, \infty\right] & \coloneqq \left]a, \infty\right[ \cup \{\infty\}, \\
    \left[-\infty, a\right] & \coloneqq \{-\infty\} \cup \left]-\infty, a\right], \\
    \left[-\infty, a\right[ & \coloneqq \{- \infty\} \cup \left]-\infty, a\right[, \\
    \intertext{sowie}
    \left]-\infty, \infty\right] & \coloneqq \set R \cup \{\infty\}, \\
    \left[-\infty, \infty\right[ & \coloneqq \{-\infty\} \cup \set R, \\
    \left]-\infty, \infty\right[ & \coloneqq \set R, \\
    \left[-\infty, \infty\right] & \coloneqq \widehat{\set R}.
  \end{align*}
\end{definition*}

\begin{comment*}
  Undefiniert und damit verboten sind die Ausdrücke
  \[
  \infty + (-\infty), (-\infty) + \infty, \infty - \infty, 0 \cdot \infty, 0 \cdot (-\infty),
  \frac{\pm \infty}{\pm \infty}.
  \]
\end{comment*}

\section{Die Einzigartigkeit des Körpers $\set Q$ der
  rationalen Zahlen}
\label{sec:q_unicity}

Die Axiome (R1)--(R12) legen den Körper $\set Q$ im wesentlichen
fest. Genauer gilt folgender Satz:
\begin{theorem*}
  Es seien $\set R$ und $\set R'$ zwei Körper, für welche neben den
  Körperaxiomen (R1)--(R9) auch die Anordnungsaxiome (R10)--(R12)
  gelten.  Nach den Konstruktionsverfahren der Abschnitte
  \ref{sec:naturals}--\ref{sec:rationals} werden auch in $\set R'$ die
  Mengen der natürlichen, ganzen und rationalen Zahlen konstruiert;
  diese Mengen seien mit $\set N_0'$, $\set Z'$ und $\set Q'$
  bezeichnet.

  Dann gibt es genau einen \emph{Körperisomorphismus} $\set Q \to \set Q'$;
  das ist eine Bijektion mit folgenden Eigenschaften für alle $a$, $b \in \set Q$:
  \begin{enumerate}
  \item
    \label{it:q_sum}
    $f(a + b) = f(a) + f(b)$,
  \item
    \label{it:q_unit}
    $f(1) = 1'$,
  \item
    \label{it:q_product}
    $f(a \cdot b) = f(a) \cdot f(b)$.
  \end{enumerate}
  Aus der Aussage \ref{it:q_sum} folgt, daß
  \begin{enumerate}[start=4]
  \item
    \label{it:q_zero}
    $f(0) = 0'$ und $f(-a) = -f(a)$.
  \end{enumerate}
  Aus den Aussagen \ref{it:q_sum}, \ref{it:q_unit} und \ref{it:q_zero} folgt
  \begin{enumerate}[start=5]
  \item
    \label{it:q_naturals}
    $f(\set N_0) = \set N_0'$.
  \end{enumerate}
  Aus den Aussagen \ref{it:q_unit} und \ref{it:q_product} folgt
  \begin{enumerate}[start=6]
    \item
    \label{it:q_division}
    $f(a) \neq 0'$ und $f(a^{-1}) = f(a)^{-1}$, wenn jeweils $a \neq 0$.
  \end{enumerate}
  Aus den Aussagen \ref{it:q_sum}--\ref{it:q_product} folgt schließlich
  \begin{enumerate}[start=7]
  \item
    \label{it:q_ordering}
    $a < b \implies f(a) < f(b)$.
  \end{enumerate}
\end{theorem*}

\begin{comment*}
  Es gibt viele Körper, welche die Axiome (R1)--(R12) erfüllen.  Jeder
  dieser Körper enthält gemäß der Abschnitte
  \ref{sec:naturals}--\ref{sec:rationals} einen Unterkörper der
  rationalen Zahlen.  Der soeben angegebene Satz besagt gerade, daß
  alle diese Unterkörper bis auf eindeutige \emph{Isomorphie} gleich
  sind.
\end{comment*}

\begin{small}
  \begin{proof}[Beweisskizze zum Theorem]
    \leavevmode
    \begin{description}
    \item[0.~Schritt.] Zunächst beweist man, daß aus der Gültigkeit von
      \ref{it:q_sum} die Aussage \ref{it:q_zero} folgt.
    \item[1.~Schritt.] Durch rekursive Definition definiert man
      $f|\set N_0$ durch $f(0) \coloneqq 0'$ und
      \[
      f(n + 1) \coloneqq f(n) + 1'
      \]
      für alle $n \in \set N_0$.  Dann beweist man durch vollständige
      Induktion die Aussagen~\ref{it:q_sum}--\ref{it:q_product} für
      alle $a$, $b \in \set N_0$, daß man $f|\set N_0$ so
      definieren muß, wenn die Aussagen~\ref{it:q_zero},
      \ref{it:q_unit} und~\ref{it:q_sum} gelten sollen, und daß
      Aussage~\ref{it:q_naturals} gilt.
    \item[2.~Schritt.] Man erweitere die Definition von $f$ auf $\set Z$ durch
      \[
      f(-n) \coloneqq -f(n)
      \]
      für $n \in \set N_1$. Dann beweist man die Aussagen
      \ref{it:q_sum} und \ref{it:q_product} für alle $a$,
      $b \in \set Z$ und daß man $f|\set Z$ so definieren muß, wenn
      die Aussage \ref{it:q_zero} gelten soll.
    \item[3.~Schritt.] Sind $n_1$, $n_2 \in \set Z$ und $m_1$, $m_2 \in \set N_1$
      und gilt $n_1/m_1 = n_2/m_2$, so zeigt man mit Aussage
      \ref{it:q_product}, daß auch $f(n_1)/f(m_1) = f(n_2)/f(m_2)$.
      Daher kann man eindeutig eine Funktion $\varphi\colon \set Q \to \set Q'$ durch
      \[
      \varphi\Bigl(\frac n m\Bigr) = \frac{f(n)}{f(m)}
      \]
      für $n \in \set Z$ und $m \in \set N_1$ definieren.  Für
      $a \in \set Z$ gilt $\varphi(a) = f(a)$.  Daher bezeichnen wir
      diese Funktion $\varphi$ jetzt mit $f$.  Für sie gelten die
      Aussagen \ref{it:q_sum}--\ref{it:q_product}.  Weiterhin folgt
      aus der Gültigkeit von \ref{it:q_product} die Aussage
      \ref{it:q_division}, und daher muß man $f$ so definieren, wenn
      die Aussagen \ref{it:q_sum}--\ref{it:q_product} gelten sollen.
    \item[4.~Schritt.] Man beweist jetzt $f(a) > 0'$ für alle $a \in \set Q_+$ und
      folgert hieraus die Aussage~\ref{it:q_ordering}, anschließend
      die Injektivität von $f$.  Die Surjektivität ergibt sich
      schließlich aus~\ref{it:q_naturals} und der Konstruktion von
      $f$.\qedhere
    \end{description}
  \end{proof}
\end{small}

%%% Local Variables:
%%% TeX-master: "skript"
%%% End:
